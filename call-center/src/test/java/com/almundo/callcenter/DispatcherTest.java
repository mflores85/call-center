package com.almundo.callcenter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

import com.almundo.callcenter.domain.Call;
import com.almundo.callcenter.domain.Dispatcher;

public class DispatcherTest 
   
{    
    
    private static final int MIN_CALL_DURATION_IN_SEC = 5;
    private static final int MAX_CALL_DURATION_IN_SEC = 10;
	
    @Test
    public void testDispatchSimpleCall() {  
    	ExecutorService executor = Executors.newSingleThreadExecutor();
    	Call call = Call.buildRandomCallInSec(MIN_CALL_DURATION_IN_SEC, MAX_CALL_DURATION_IN_SEC);    	
    	Dispatcher dispatcher = new Dispatcher();
    	dispatcher.dispatchCall(call);
    	executor.execute(dispatcher);
    }
    
    @Test
    public void testDispatch7CallsToOperators()  {
		ExecutorService executor = Executors.newFixedThreadPool(7);		
    	Dispatcher dispatcher = new Dispatcher();        	
    	Call.buildRandomCallListInSec(7, MIN_CALL_DURATION_IN_SEC, MAX_CALL_DURATION_IN_SEC)
    	    	
    	.stream().forEach(call -> { 
    		dispatcher.dispatchCall(call); 
    		executor.execute(dispatcher);    		   			
    	});   	
    }    
    
    @Test
    public void testDispatch10Calls()  {
		ExecutorService executor = Executors.newFixedThreadPool(10);		
    	Dispatcher dispatcher = new Dispatcher();        	
    	Call.buildRandomCallListInSec(10, MIN_CALL_DURATION_IN_SEC, MAX_CALL_DURATION_IN_SEC)
    	    	
    	.stream().forEach(call -> { 
    		dispatcher.dispatchCall(call); 
    		executor.execute(dispatcher);    		   			
    	});   	
    }  
}
