package com.almundo.callcenter.enums;

public enum EmployeeType {
    OPERATOR(1),
    SUPERVISOR(2),
    DIRECTOR(3);

    private int value;	
    
    private EmployeeType(int value) {
        this.value = value;
    }

    public int getValue() { return value; }
}
