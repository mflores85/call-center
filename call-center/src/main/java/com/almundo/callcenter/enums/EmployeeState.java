package com.almundo.callcenter.enums;

public enum EmployeeState {
    AVAILABLE,
    BUSY
}
