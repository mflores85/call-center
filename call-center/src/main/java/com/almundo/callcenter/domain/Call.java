package com.almundo.callcenter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Call {
	
	private Integer durationInSec;
	
    public Integer getDurationInSec() {
        return durationInSec;
    }
	
    public Call(Integer durationInSec) {        
        this.durationInSec = durationInSec;
    }
    
    public static Call buildRandomCallInSec(Integer minDurationInSec, Integer maxDurationInSec) {        
        return new Call(ThreadLocalRandom.current().nextInt(minDurationInSec, maxDurationInSec + 1));
    }
    
    public static List<Call> buildRandomCallListInSec(Integer listSize, Integer minDurationInSec, Integer maxDurationInSec) {
        List<Call> callList = new ArrayList<>();
        for (int i = 0; i < listSize; i++) {
            callList.add(buildRandomCallInSec(minDurationInSec, maxDurationInSec));
        }
        return callList;
    }
}
