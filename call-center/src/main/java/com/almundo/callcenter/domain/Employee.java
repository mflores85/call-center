package com.almundo.callcenter.domain;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.almundo.callcenter.enums.EmployeeState;
import com.almundo.callcenter.enums.EmployeeType;

public class Employee implements Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(Employee.class);
	private EmployeeType type;
	private EmployeeState state;
	private ConcurrentLinkedQueue<Call> pendingCalls;
	private Boolean active;		
	
	public Employee(EmployeeType type) {
		this.type = type;
		this.state = EmployeeState.AVAILABLE;
		this.pendingCalls = new ConcurrentLinkedQueue<Call>();
		this.active = true; 
	}
	
	
	public synchronized void answerPhone(Call call)  {
		this.pendingCalls.add(call);
	}
	
	public  Integer getOrder() {
		return type.getValue();
	}
		
	@Override
	public void run() {
		while(active) {
			if(!this.pendingCalls.isEmpty()) {
				Call call = pendingCalls.poll();
				if(call != null) {					
					try {
						logger.info("Llamada atendida por " + this.type);
						this.setState(EmployeeState.BUSY);
						TimeUnit.SECONDS.sleep(call.getDurationInSec());
						this.setState(EmployeeState.AVAILABLE);
						logger.info("Llamada finalizada por " + this.type);
					} catch (InterruptedException e) {			
						logger.error("Error al contestar llamada", e);
					}						
				}	
			}
		}	
	}
	
	public EmployeeState getState() {
		return state;
	}

	public synchronized void setState(EmployeeState state) {
		this.state = state;
	}

	public EmployeeType getType() {
		return type;
	}

	public void setState(EmployeeType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "Employee [type=" + type + ", state=" + state + "]";
	}	
}
