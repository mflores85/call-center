package com.almundo.callcenter.domain;

import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.almundo.callcenter.enums.EmployeeState;
import com.almundo.callcenter.enums.EmployeeType;


public class Dispatcher implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);
	public static final Integer MAX_THREADS = 10;	
	private Vector<Employee> employees;	
	private ConcurrentLinkedQueue<Call> pendingCalls;	
	private Boolean active;	
	private ExecutorService executorService;
		
	public Dispatcher() {
		super();
		pendingCalls = new ConcurrentLinkedQueue<>();
		employees = new Vector<Employee>();
		setDefaultEmployees();
		active = true;
		executorService = Executors.newFixedThreadPool(MAX_THREADS);		
	}
 
	private  void setDefaultEmployees() {
		employees.add(new Employee(EmployeeType.DIRECTOR));
		employees.add(new Employee(EmployeeType.SUPERVISOR));
		employees.add(new Employee(EmployeeType.SUPERVISOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
		employees.add(new Employee(EmployeeType.OPERATOR));
	}
	
	public synchronized void dispatchCall(Call call){	
		logger.info("Nueva llamada con duracion : " + call.getDurationInSec());
		pendingCalls.add(call);
	}

	public void setEmployees(Vector<Employee> employees) {    	
		this.employees = employees;		
	}

	@Override
	public void run() {
		while(active) {			
			if(!pendingCalls.isEmpty()) {				
				Call call = pendingCalls.poll();
				if (call != null) {					
					if (!employees.isEmpty()) {
						Employee employee = getEmployee();
						if (employee != null) {
							employee.answerPhone(call);
							executorService.execute(employee);
						}
					}
				}					
			}			
		}		
	}

	public synchronized Employee getEmployee() {		 
		 List<Employee> availableEmployees = this.employees.stream().filter(e -> e.getState().equals(EmployeeState.AVAILABLE)).collect(Collectors.toList());

	      Optional<Employee> employee = availableEmployees.stream().filter(e -> e.getType() == EmployeeType.OPERATOR).findAny();
	      if (!employee.isPresent()) {
	          employee = availableEmployees.stream().filter(e -> e.getType() == EmployeeType.SUPERVISOR).findAny();
	          if (!employee.isPresent()) {
	              employee = availableEmployees.stream().filter(e -> e.getType() == EmployeeType.DIRECTOR).findAny();
	              if (!employee.isPresent()) {
	                  return null;
	              }
	          }
	      }	      
		 return  employee.get();
	}	
}
